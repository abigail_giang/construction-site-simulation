package simulation.domain.cost;

import org.junit.After;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.io.ByteArrayOutputStream;
import java.io.OutputStream;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class StdOutFormatterTest {
    @Mock
    private CostCalculator cal;

    private final StdOutFormatter formatter = new StdOutFormatter();
    private final PrintStream sysOut = System.out;

    @Test
    public void test_should_print_correct_record() {
        OutputStream outS = new ByteArrayOutputStream();
        PrintStream ps = new PrintStream(outS);

        List<ReportCostItem> items = new ArrayList<>(5);
        items.add(new ReportCostItem("Fuel consumption cost", 20, 1));
        items.add(new ReportCostItem("Communication cost", 5, 1));
        items.add(new ReportCostItem("Damage cost", 2, 2));
        items.add(new ReportCostItem("Destruction cost", 1, 10));
        items.add(new ReportCostItem("Uncleared square cost", 10, 3));
        when(cal.getTotalCost()).thenReturn(59);

        when(cal.getReportCostItems()).thenReturn(items);
        System.setOut(ps);
        formatter.createReport(cal);

        String expected =
                "Item                          |\tQuantity\tCost\n" +
                        "Fuel consumption cost         |\t20\t          20\n" +
                        "Communication cost            |\t5 \t           5\n" +
                        "Damage cost                   |\t2 \t           4\n" +
                        "Destruction cost              |\t1 \t          10\n" +
                        "Uncleared square cost         |\t10\t          30\n" +
                        "--- Total                     |\t                  59";
        assertThat("Should print correct report format", outS.toString().trim(), equalTo(expected.trim()));
    }

    @After
    public void tearDown() {
        System.out.flush();
        System.setOut(sysOut);
    }
}