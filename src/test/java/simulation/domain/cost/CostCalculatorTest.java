package simulation.domain.cost;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.MockitoJUnitRunner;
import simulation.domain.construction.Bulldozer;
import simulation.domain.construction.ConstructionSiteMap;
import simulation.domain.site.*;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.Mockito.when;
import static simulation.domain.cost.Constants.*;

@RunWith(MockitoJUnitRunner.class)
public class CostCalculatorTest {
    @Mock
    private ConstructionSiteMap map;
    @Mock
    private Bulldozer bulldozer;
    @Spy
    private final TreeLand tl = new TreeLand();
    @Spy
    private final ProtectedLand pl = new ProtectedLand();
    @Spy
    private final RockyLand rl = new RockyLand();

    private CostCalculator costCal;

    @Before
    public void setup() {
        LandBlock[][] landMap = new LandBlock[2][2];
        for (int x = 0; x < 2; x++) {
            for (int y = 0; y < 2; y++) {
                landMap[y][x] = new PlainLand();
            }
        }
        landMap[0][0] = tl;
        landMap[0][1] = rl;
        landMap[1][1] = pl;

        when(map.getCopyMap()).thenReturn(landMap);
        when(bulldozer.getConstructionSite()).thenReturn(map);
        costCal = new CostCalculator(bulldozer);
    }

    @Test
    public void test_correct_damage_cost() {
        when(tl.getDamageQty()).thenReturn(2);
        int actual = costCal.getDamageCost().getTotalCost();
        assertThat("Should get the correct damage cost: ", actual, equalTo(2 * DAMAGE_COST));
    }

    @Test
    public void test_correct_fuel_consumption_cost() {
        when(tl.getFuelQty()).thenReturn(5);
        when(pl.getFuelQty()).thenReturn(1);
        when(rl.getFuelQty()).thenReturn(2);
        int actual = costCal.getFuelUsage().getTotalCost();
        assertThat("Should get the correct fuel consumption cost: ", actual, equalTo(8 * FUEL_COST));
    }

    @Test
    public void test_correct_destruction_cost() {
        when(pl.isDestructed()).thenReturn(true);
        int actual = costCal.getDestructionCost().getTotalCost();
        assertThat("Should get the correct destruction cost: ", actual, equalTo(DESTRUCTION_COST));
    }

    @Test
    public void test_correct_communication_overhead() {
        when(bulldozer.getCmdCount()).thenReturn(3);
        int actual = costCal.getCommunicationOverhead().getTotalCost();
        assertThat("Should get the correct communication overhead: ", actual, equalTo(3 * COMMUNICATION_COST));
    }

    @Test
    public void test_correct_uncleared_cost_excl_protected_land() {
        when(tl.isCleared()).thenReturn(true);
        int actual = costCal.getUnclearedCost().getTotalCost();
        assertThat("Should get the correct uncleared cost: ", actual, equalTo(2 * UNCLEARED_COST));
    }
}
