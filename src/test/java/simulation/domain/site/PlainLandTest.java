package simulation.domain.site;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Spy;
import org.mockito.junit.MockitoJUnitRunner;

import static org.hamcrest.core.IsEqual.equalTo;
import static org.junit.Assert.assertThat;

@RunWith(MockitoJUnitRunner.class)
public class PlainLandTest {
    @Spy
    private PlainLand pl;

    @Test
    public void test_should_incur_correct_cost_clearing_land() {
        pl.incurCost();
        assertThat("Should incur 1 fuel unit for clearing plain land", pl.getFuelQty(), equalTo(1));
    }
}
