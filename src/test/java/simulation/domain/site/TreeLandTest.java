package simulation.domain.site;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Spy;
import org.mockito.junit.MockitoJUnitRunner;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class TreeLandTest {
    @Spy
    private TreeLand tl;

    @Test
    public void test_incur_cost_when_passing_not_cleared() {
        tl.incurCost();
        assertThat("Should incur 2 fuels clearing tree land", tl.getFuelQty(), equalTo(2));
        assertThat("Should incur damage costs passing uncleared tree land", tl.getDamageQty(), equalTo(1));
        assertThat("Should be cleared after passing", tl.isCleared());
    }

    @Test
    public void test_passing_after_cleared() {
        when(tl.isCleared()).thenReturn(true);
        tl.incurCost();
        assertThat("Should incur 1 fuel passing cleared tree land", tl.getFuelQty(), equalTo(1));
        assertThat("Should not incur damage cost", tl.getDamageQty(), equalTo(0));
    }

    @Test
    public void test_does_not_incur_damage_cost_when_stop_on_this() {
        tl.stopOnThis = true;
        tl.incurCost();
        assertThat("Should incur clearing cost of 2 fuels for tree land", tl.getFuelQty(), equalTo(2));
        assertThat("Should not incur damage cost when clearing target tree land", tl.getDamageQty(), equalTo(0));
    }
}
