package simulation.domain.site;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;

@RunWith(MockitoJUnitRunner.class)
public class LandBlockTest {

    @Test
    public void test_create_tree_land_block_from_text() {
        final LandBlock t = LandBlock.of("t");
        assertThat("Should create a tree land for 't'", t.isType(), equalTo(Sign.TREE));
        assertThat(t.toString(), equalTo("t"));
    }

    @Test
    public void test_create_protected_tree_land_block_from_text() {
        final LandBlock t = LandBlock.of("T");
        assertThat("Should create a protected tree land for 'T'", t.isType(), equalTo(Sign.PROTECTED_TREE));
        assertThat(t.toString(), equalTo("T"));
    }

    @Test
    public void test_create_protected_rocky_land_from_text() {
        final LandBlock t = LandBlock.of("r");
        assertThat("Should create a rocky land for 'r'", t.isType(), equalTo(Sign.ROCKY));
        assertThat(t.toString(), equalTo("r"));
    }

    @Test
    public void test_create_plain_land_from_text() {
        final LandBlock t = LandBlock.of("o");
        assertThat("Should create a plain land for 'o'", t.isType(), equalTo(Sign.PLAIN));
        assertThat(t.toString(), equalTo("o"));
    }

}