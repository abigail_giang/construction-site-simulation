package simulation.domain.site;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Spy;
import org.mockito.junit.MockitoJUnitRunner;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class RockyLandTest {
    @Spy
    private RockyLand rl;

    @Test
    public void test_clearing_rockly_land_should_incur_correct_costs() {
        rl.incurCost();
        assertThat("Should incur 2 fuel units for clearing rocky land", rl.getFuelQty(), equalTo(2));
    }

    @Test
    public void test_passing_cleared_rockly_land_should_incur_correct_cost() {
        when(rl.isCleared()).thenReturn(true);
        rl.incurCost();
        assertThat("Should incur 1 fuel unit passing cleared rocky land", rl.getFuelQty(), equalTo(1));
    }
}