package simulation.domain.site;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.Spy;
import org.mockito.junit.MockitoJUnitRunner;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;

@RunWith(MockitoJUnitRunner.class)
public class ProtectedLandTest {
    @Spy
    private ProtectedLand pl;

    @Rule
    public ExpectedException ex = ExpectedException.none();

    @Test
    public void test_should_incur_cost_clearing_protected_land() throws IllegalDestructionException {
        ex.expect(IllegalDestructionException.class);
        pl.incurCost();
        assertThat("Should incur visiting cost", pl.getFuelQty(), equalTo(1));
        assertThat("Should incur destruction cost", pl.isDestructed(), equalTo(true));
    }
}