package simulation.domain.construction;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import static org.junit.Assert.assertEquals;
import static simulation.domain.construction.Direction.*;

@RunWith(MockitoJUnitRunner.class)
public class RightCommandTest {
    @Mock
    private ConstructionSiteMap map;

    private final Bulldozer bulldozer = new Bulldozer(map);

    private final RightCommand rightCmd = new RightCommand();

    @Test
    public void test_turn_right_when_down() {
        bulldozer.currentDirection = DOWN;
        rightCmd.execute(bulldozer);
        assertEquals("When moving down, turning right should be moving backward", bulldozer.currentDirection, BACKWARD);
    }

    @Test
    public void test_turn_right_when_forward() {
        bulldozer.currentDirection = FORWARD;
        rightCmd.execute(bulldozer);
        assertEquals("When moving forward, turning right should be moving down", bulldozer.currentDirection, DOWN);
    }

    @Test
    public void test_turn_right_when_up() {
        bulldozer.currentDirection = UP;
        rightCmd.execute(bulldozer);
        assertEquals("When moving up, turning right should be moving forward", bulldozer.currentDirection, FORWARD);
    }

    @Test
    public void test_turn_right_when_backward() {
        bulldozer.currentDirection = BACKWARD;
        rightCmd.execute(bulldozer);
        assertEquals("When moving backward, turning right should be moving up", bulldozer.currentDirection, UP);
    }
}