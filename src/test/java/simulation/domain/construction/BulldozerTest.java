package simulation.domain.construction;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import simulation.domain.site.IllegalDestructionException;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.Mockito.verify;

@RunWith(MockitoJUnitRunner.class)
public class BulldozerTest {
    @Mock
    private LeftCommand lCmd;
    @Mock
    private ConstructionSiteMap map;

    private final Bulldozer bulldozer = new Bulldozer(map);

    @Test
    public void test_bulldozer_cmd_count_should_increase_for_execute()
            throws BulldozerInvalidMoveException, IllegalDestructionException {
        bulldozer.executeCmd(lCmd);
        assertThat("Should increase command count", bulldozer.getCmdCount(), equalTo(1));
        verify(lCmd).execute(bulldozer);
    }
}
