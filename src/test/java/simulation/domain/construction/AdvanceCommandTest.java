package simulation.domain.construction;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.MockitoJUnitRunner;
import simulation.domain.site.IllegalDestructionException;
import simulation.domain.site.LandBlock;
import simulation.domain.site.PlainLand;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class AdvanceCommandTest {
    @Mock
    private ConstructionSiteMap map;
    @Spy
    private final Bulldozer bulldozer = new Bulldozer(map);

    @Before
    public void setup() {
        LandBlock[][] landMap = new LandBlock[9][9];
        for (int x = 0; x < 9; x++) {
            for (int y = 0; y < 9; y++) {
                landMap[y][x] = new PlainLand();
            }
        }
        when(bulldozer.getWorkMap()).thenReturn(map);
        when(map.getMap()).thenReturn(landMap);
    }

    @Test
    public void test_move_forward_3_steps_at_program_start()
            throws IllegalDestructionException, BulldozerInvalidMoveException {

        AdvanceCommand cmd = new AdvanceCommand("3");
        cmd.execute(bulldozer);
        assertEquals("Should move to the new position X", bulldozer.posX, 2);
        assertEquals("Position Y should be 0", bulldozer.posY, 0);
        assertThat("Last block should be flagged", map.getMap()[bulldozer.posY][bulldozer.posX].stopOnThis, equalTo(true));
    }

    @Test
    public void test_move_forward_from_inside_site()
            throws BulldozerInvalidMoveException, IllegalDestructionException {
        AdvanceCommand cmd = new AdvanceCommand("4");
        bulldozer.posX = 3;
        bulldozer.posY = 1;
        cmd.execute(bulldozer);
        assertEquals("Should move bulldozer to the new position X", bulldozer.posX, 7);
        assertEquals("Should remain at the same position Y", bulldozer.posY, 1);
        assertThat("Last block should be flagged", map.getMap()[bulldozer.posY][bulldozer.posX].stopOnThis, equalTo(true));
    }

    @Test
    public void test_move_backward()
            throws BulldozerInvalidMoveException, IllegalDestructionException {
        AdvanceCommand cmd = new AdvanceCommand("5");
        bulldozer.currentDirection = Direction.BACKWARD;
        bulldozer.posX = 7;
        bulldozer.posY = 4;
        cmd.execute(bulldozer);
        assertEquals("Should move bulldozer to the new position X", 2, bulldozer.posX);
        assertEquals("Should remain at the same position Y", 4, bulldozer.posY);
        assertThat("Last block should be flagged", map.getMap()[bulldozer.posY][bulldozer.posX].stopOnThis, equalTo(true));
    }

    @Test
    public void test_move_up()
            throws BulldozerInvalidMoveException, IllegalDestructionException {
        AdvanceCommand cmd = new AdvanceCommand("3");
        bulldozer.currentDirection = Direction.UP;
        bulldozer.posX = 3;
        bulldozer.posY = 8;
        cmd.execute(bulldozer);
        assertEquals("Should remain bulldozer position X", bulldozer.posX, 3);
        assertEquals("Should move bulldozer to the new position Y", bulldozer.posY, 5);
        assertThat("Last block should be flagged", map.getMap()[bulldozer.posY][bulldozer.posX].stopOnThis, equalTo(true));
    }

    @Test
    public void test_move_down()
            throws BulldozerInvalidMoveException, IllegalDestructionException {
        AdvanceCommand cmd = new AdvanceCommand("5");
        bulldozer.currentDirection = Direction.DOWN;
        bulldozer.posX = 3;
        bulldozer.posY = 2;
        cmd.execute(bulldozer);
        assertEquals("Should remain bulldozer position X", 3, bulldozer.posX);
        assertEquals("Should move bulldozer to new position Y", 7, bulldozer.posY);
        assertThat("Last block should be flagged", map.getMap()[bulldozer.posY][bulldozer.posX].stopOnThis, equalTo(true));
    }

    @Rule
    public ExpectedException ex = ExpectedException.none();

    @Test
    public void test_move_out_of_bound_X() throws BulldozerInvalidMoveException, IllegalDestructionException {
        ex.expect(BulldozerInvalidMoveException.class);
        ex.expectMessage("Move invalid, exceed site boundary. Program terminated.");
        AdvanceCommand cmd = new AdvanceCommand("6");
        bulldozer.posX = 3;
        cmd.execute(bulldozer);
    }

    @Test
    public void test_move_out_of_bound_Y() throws BulldozerInvalidMoveException, IllegalDestructionException {
        ex.expect(BulldozerInvalidMoveException.class);
        ex.expectMessage("Move invalid, exceed site boundary. Program terminated.");
        AdvanceCommand cmd = new AdvanceCommand("6");
        bulldozer.posX = 3;
        cmd.execute(bulldozer);
    }
}