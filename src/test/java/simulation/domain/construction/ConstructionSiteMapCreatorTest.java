package simulation.domain.construction;

import org.junit.Test;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;

public class ConstructionSiteMapCreatorTest {
    @Test
    public void test_construction_sitemap_create_from_string() {
        ConstructionSiteMapCreator creator = new ConstructionSiteMapCreator();
        String map = "o o t o o r o o o\n" +
                "o o T T o o r o t\n" +
                "t t t o o o o T r";
        final ConstructionSiteMap csm = creator.createMap(map);
        String actual = csm.printMap();
        assertThat("Should print the correct map.", actual, equalTo(map.trim()));
    }
}