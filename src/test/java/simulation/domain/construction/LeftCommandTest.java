package simulation.domain.construction;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import static org.junit.Assert.assertEquals;
import static simulation.domain.construction.Direction.*;

@RunWith(MockitoJUnitRunner.class)
public class LeftCommandTest {
    @Mock
    private ConstructionSiteMap map;

    private final Bulldozer bulldozer = new Bulldozer(map);

    private final LeftCommand leftCmd = new LeftCommand();

    @Test
    public void test_turn_left_when_down() {
        bulldozer.currentDirection = DOWN;
        leftCmd.execute(bulldozer);
        assertEquals("When moving down, turning left should be moving forward", bulldozer.currentDirection, FORWARD);
    }

    @Test
    public void test_turn_left_when_forward() {
        bulldozer.currentDirection = FORWARD;
        leftCmd.execute(bulldozer);
        assertEquals("When moving forward, turning left should be moving up", bulldozer.currentDirection, UP);
    }

    @Test
    public void test_turn_left_when_up() {
        bulldozer.currentDirection = UP;
        leftCmd.execute(bulldozer);
        assertEquals("When moving up, turning left should be moving backward", bulldozer.currentDirection, BACKWARD);
    }

    @Test
    public void test_turn_left_when_backward() {
        bulldozer.currentDirection = BACKWARD;
        leftCmd.execute(bulldozer);
        assertEquals("When moving backward, turning left should be moving down", bulldozer.currentDirection, DOWN);
    }
}