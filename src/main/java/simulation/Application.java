package simulation;

import simulation.domain.construction.*;
import simulation.domain.cost.CostCalculator;
import simulation.domain.cost.StdOutFormatter;
import simulation.domain.site.IllegalDestructionException;

import java.util.Scanner;

public class Application {
    public static void main(String[] args) {
        System.out.println("============== STARTING SIMULATION PROGRAM ==============");
        ConstructionSiteMapCreator creator = new ConstructionSiteMapCreator();
        ConstructionSiteMap map = creator.createMap(
                "ootooooooo\n" +
                        "oooooooToo\n" +
                        "rrrooooToo\n" +
                        "rrrroooooo\n" +
                        "rrrrrtoooo");
        System.out.println(map.printMap());
        System.out.println("================== ENTER YOUR COMMANDS ==================");
        Scanner s = new Scanner(System.in);
        Bulldozer bulldozer = new Bulldozer(map);
        StringBuilder cmdHistory = new StringBuilder();
        try {
            do {
                System.out.println("<q> to quit, <l> for left, <r> for right, <a> for advance <n> steps: ");
                String input = s.nextLine();
                DirectionCommand cmd;
                if (input.startsWith("q")) {
                    cmdHistory.append("q").append(", ");
                    break;
                } else if (input.startsWith("l")) {
                    cmd = new LeftCommand();
                } else if (input.startsWith(("r"))) {
                    cmd = new RightCommand();
                } else if (input.startsWith("a")) {
                    try {
                        cmd = new AdvanceCommand(input.split(" ")[1]);
                    } catch (IllegalArgumentException ex) {
                        System.out.println(ex.getMessage());
                        continue;
                    } catch (ArrayIndexOutOfBoundsException ex) {
                        System.out.println("Invalid format, eg: a 2, to move 2 steps");
                        continue;
                    }
                } else {
                    System.out.println("You have entered an invalid command.");
                    continue;
                }
                cmdHistory.append(cmd).append(", ");
                bulldozer.executeCmd(cmd);
            } while (true);
        } catch (BulldozerInvalidMoveException | IllegalDestructionException ex) {
            System.out.println(ex.getMessage());
        }

        CostCalculator cal = new CostCalculator(bulldozer);
        System.out.println(
                "\nThe simulation has ended at your request. These are the commands you issued: \n" +
                        cmdHistory.delete(cmdHistory.length() - 2, cmdHistory.length() - 1).toString()
        );

        System.out.println("\nThe costs for this land clearing operation were:");
        System.out.println("---------------------------------------------------");

        cal.createCostReport(new StdOutFormatter());

        System.out.println("\n---------------------------------------------------");
        System.out.println("Thank you for using the Aconex site clearing simulator.");
    }
}
