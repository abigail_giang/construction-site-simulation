package simulation.domain.site;

public class TreeLand extends LandBlock {

    private static final int CLEARING_FUEL = 2;
    private int damageQty = 0;

    @Override
    public Sign isType() {
        return Sign.TREE;
    }

    public int getDamageQty() {
        return damageQty;
    }

    @Override
    public void incurCost() {
        if (!isCleared()) {
            fuelQty += CLEARING_FUEL;
            if (!stopOnThis) {
                damageQty++;
            }
            return;
        }
        fuelQty++;
    }

    @Override
    public String toString() {
        return "t";
    }
}
