package simulation.domain.site;

public class IllegalDestructionException extends Throwable {
    public IllegalDestructionException(String message) {
        super(message);
    }
}
