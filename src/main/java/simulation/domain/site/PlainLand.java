package simulation.domain.site;

public class PlainLand extends LandBlock {
    @Override
    public Sign isType() {
        return Sign.PLAIN;
    }

    @Override
    public void incurCost() {
        fuelQty++;
    }

    @Override
    public String toString() {
        return "o";
    }
}
