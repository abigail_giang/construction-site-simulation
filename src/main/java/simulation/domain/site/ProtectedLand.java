package simulation.domain.site;

public class ProtectedLand extends LandBlock {
    private boolean destructed = false;

    @Override
    public Sign isType() {
        return Sign.PROTECTED_TREE;
    }

    @Override
    public void incurCost() throws IllegalDestructionException {
        destructed = true;
        fuelQty++;
        throw new IllegalDestructionException("You have destroyed a protected land.");
    }

    public boolean isDestructed() {
        return destructed;
    }

    @Override
    public String toString() {
        return "T";
    }
}
