package simulation.domain.site;

public abstract class LandBlock {
    int fuelQty = 0;
    public boolean stopOnThis = false;

    public static LandBlock of(String s) {
        if (Sign.ROCKY.toString().equals(s)) {
            return new RockyLand();
        } else if (Sign.TREE.toString().equals(s)) {
            return new TreeLand();
        } else if (Sign.PROTECTED_TREE.toString().equals(s)) {
            return new ProtectedLand();
        }
        return new PlainLand();
    }

    public abstract Sign isType();

    public boolean isCleared() {
        return getFuelQty() > 0;
    }

    public int getFuelQty() {
        return fuelQty;
    }

    public abstract void incurCost() throws IllegalDestructionException;
}
