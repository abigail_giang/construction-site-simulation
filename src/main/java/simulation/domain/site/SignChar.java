package simulation.domain.site;

public enum SignChar {
    ROCKY('r'),
    TREE('t'),
    PROTECTED('T'),
    PLAIN('o');

    private final char c;

    SignChar(char c) {
        this.c = c;
    }

    @Override
    public String toString() {
        return String.valueOf(c);
    }

}
