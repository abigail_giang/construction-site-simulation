package simulation.domain.site;

public enum Sign {
    ROCKY("r"), PLAIN("o"), TREE("t"), PROTECTED_TREE("T");

    private final String mapSign;

    Sign(String c) {
        this.mapSign = c;
    }

    @Override
    public String toString() {
        return mapSign;
    }
}
