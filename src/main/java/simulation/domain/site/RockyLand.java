package simulation.domain.site;

public class RockyLand extends LandBlock {
    private static final int CLEARING_FUEL_QTY = 2;

    @Override
    public Sign isType() {
        return Sign.ROCKY;
    }

    @Override
    public void incurCost() {
        if (isCleared()) {
            fuelQty++;
            return;
        }
        fuelQty += CLEARING_FUEL_QTY;
    }

    @Override
    public String toString() {
        return "r";
    }
}
