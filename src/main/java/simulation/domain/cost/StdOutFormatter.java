package simulation.domain.cost;

public class StdOutFormatter implements ReportFormatter {

    @Override
    public void createReport(CostCalculator calculator) {
        StringBuilder sb = new StringBuilder();
        sb.append(String.format("%1$-30s|\t%2$-2s\t%3$2s\n", "Item", "Quantity", "Cost"));
        for (ReportCostItem i : calculator.getReportCostItems()) {
            sb.append(String.format("%1$-30s|\t%2$-2s\t%3$12s\n", i.getName(), i.getQuantity(), i.getTotalCost()));
        }
        sb.append(String.format("%1$-30s|\t%2$20s", "--- Total", calculator.getTotalCost()));
        System.out.print(sb.toString().trim());
    }
}