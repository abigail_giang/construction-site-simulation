package simulation.domain.cost;

public interface ReportFormatter {
    void createReport(CostCalculator calculator);
}
