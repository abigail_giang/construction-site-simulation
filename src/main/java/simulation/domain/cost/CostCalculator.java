package simulation.domain.cost;

import simulation.domain.construction.Bulldozer;
import simulation.domain.site.LandBlock;
import simulation.domain.site.ProtectedLand;
import simulation.domain.site.Sign;
import simulation.domain.site.TreeLand;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import static simulation.domain.cost.Constants.*;

/**
 * This class will calculate all the costs
 */
public class CostCalculator {
    private final List<LandBlock> landBlock;
    private final Bulldozer bulldozer;
    private final List<ReportCostItem> items = new ArrayList<>();

    public CostCalculator(Bulldozer bulldozer) {
        this.bulldozer = bulldozer;
        this.landBlock = Arrays.stream(bulldozer.getConstructionSite().getCopyMap())
                .flatMap(Arrays::stream).collect(Collectors.toList());
    }

    ReportCostItem getDamageCost() {
        final int quantity = landBlock.stream().filter(lb -> lb.isType() == Sign.TREE)
                .map(lb -> ((TreeLand) lb).getDamageQty())
                .reduce(0, Integer::sum);
        return new ReportCostItem("paint damage to bulldozer", quantity, DAMAGE_COST);
    }

    ReportCostItem getFuelUsage() {
        final int quantity = landBlock.stream().map(LandBlock::getFuelQty).reduce(0, Integer::sum);
        return new ReportCostItem("fuel usage", quantity, FUEL_COST);
    }

    ReportCostItem getDestructionCost() {

        final int quantity = (int) landBlock.stream().filter(lb -> lb.isType() == Sign.PROTECTED_TREE)
                .filter(lb -> ((ProtectedLand) lb).isDestructed()).count();
        return new ReportCostItem("destruction of protected tree", quantity, DESTRUCTION_COST);
    }

    ReportCostItem getCommunicationOverhead() {
        return new ReportCostItem("communication overhead", bulldozer.getCmdCount(), COMMUNICATION_COST);
    }

    ReportCostItem getUnclearedCost() {
        final int quantity = (int) (landBlock.stream()
                .filter(lb -> !lb.isCleared() && lb.isType() != Sign.PROTECTED_TREE)
                .count());
        return new ReportCostItem("uncleared squares", quantity, UNCLEARED_COST);
    }

    int getTotalCost() {
        return items.stream().map(ReportCostItem::getTotalCost).reduce(Integer::sum).orElse(0);
    }

    List<ReportCostItem> getReportCostItems() {
        return List.copyOf(items);
    }

    public void createCostReport(ReportFormatter formatter) {
        items.add(getCommunicationOverhead());
        items.add(getFuelUsage());
        items.add(getUnclearedCost());
        items.add(getDestructionCost());
        items.add(getDamageCost());
        formatter.createReport(this);
        items.clear();
    }
}
