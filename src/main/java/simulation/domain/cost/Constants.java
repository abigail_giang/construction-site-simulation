package simulation.domain.cost;

public class Constants {
    static final int COST_UNIT = 1;
    static final int FUEL_COST = COST_UNIT;
    static final int UNCLEARED_COST = 3 * COST_UNIT;
    static final int DESTRUCTION_COST = 10 * COST_UNIT;
    static final int DAMAGE_COST = 2 * COST_UNIT;
    static final int COMMUNICATION_COST = COST_UNIT;
}
