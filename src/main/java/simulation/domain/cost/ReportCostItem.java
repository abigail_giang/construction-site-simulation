package simulation.domain.cost;

class ReportCostItem {
    private final String name;
    private final int quantity;
    private final int itemCost;

    ReportCostItem(String name, int quantity, int itemCost) {
        this.name = name;
        this.quantity = quantity;
        this.itemCost = itemCost;
    }

    int getTotalCost() {
        return quantity * itemCost;
    }

    int getQuantity() {
        return quantity;
    }

    String getName() {
        return name;
    }
}
