package simulation.domain.construction;

import simulation.domain.site.IllegalDestructionException;

public interface DirectionCommand {
    void execute(Bulldozer bulldozer) throws IllegalDestructionException, BulldozerInvalidMoveException;
}
