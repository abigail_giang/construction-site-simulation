package simulation.domain.construction;

import simulation.domain.site.LandBlock;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Scanner;

public class ConstructionSiteMapCreator {
    public ConstructionSiteMap createMap(String mapStr) {
        final String[] rows = mapStr.split("\n");
        final int h = rows.length;
        final LandBlock[][] map = new LandBlock[h][];
        Scanner s = new Scanner(mapStr);

        for (int y = 0; y < h; y++) {
            final ArrayList<String> l = new ArrayList<>(List.of(s.nextLine().split("")));
            l.removeAll(Collections.singleton(" "));
            int w = l.size();
            map[y] = new LandBlock[w];
            for (int x = 0; x < l.size(); x++) {
                final LandBlock lb = LandBlock.of(l.get(x));
                map[y][x] = lb;
            }
        }
        return new ConstructionSiteMap(map);
    }
}
