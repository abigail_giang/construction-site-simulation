package simulation.domain.construction;

public class BulldozerInvalidMoveException extends Throwable {
    public BulldozerInvalidMoveException(String message, Throwable e) {
        super(message, e);
    }
}
