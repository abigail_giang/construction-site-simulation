package simulation.domain.construction;

import simulation.domain.site.IllegalDestructionException;
import simulation.domain.site.LandBlock;

import java.util.HashMap;
import java.util.Map;

import static simulation.domain.construction.Direction.*;

public class AdvanceCommand implements DirectionCommand {
    private final int step;
    private ConstructionSiteMap map;
    private Bulldozer bulldozer;
    private static final Map<Direction, Integer> move = new HashMap<>() {{
        put(UP, -1);
        put(DOWN, 1);
        put(BACKWARD, -1);
        put(FORWARD, 1);
    }};

    public AdvanceCommand(String step) throws IllegalArgumentException {
        try {
            this.step = Integer.parseInt(step);
            if (this.step < 1) {
                throw new IllegalArgumentException("Incorrect number of steps");
            }
        } catch (NumberFormatException e) {
            throw new IllegalArgumentException("Incorrect correct number of steps", e);
        }
    }

    @Override
    public void execute(Bulldozer bulldozer) throws BulldozerInvalidMoveException, IllegalDestructionException {
        map = bulldozer.getWorkMap();
        this.bulldozer = bulldozer;
        switch (bulldozer.currentDirection) {
            case FORWARD:
            case BACKWARD:
                moveHorizontal();
                break;
            case UP:
            case DOWN:
                moveVertical();
                break;
        }
    }

    private void moveHorizontal() throws BulldozerInvalidMoveException, IllegalDestructionException {
        int s = move.get(bulldozer.currentDirection);
        int start = Math.min(0, step * s);
        int end = Math.max(0, step * s);
        LandBlock lb;
        try {
            for (int a = start; a < end; a++) {
                bulldozer.posX += s;
                lb = map.getMap()[bulldozer.posY][bulldozer.posX];
                lb.stopOnThis = a == end - 1;
                lb.incurCost();
            }
        } catch (ArrayIndexOutOfBoundsException e) {
            throw new BulldozerInvalidMoveException("Move invalid, exceed site boundary. Program terminated.", e);
        }
    }

    private void moveVertical() throws BulldozerInvalidMoveException, IllegalDestructionException {
        int s = move.get(bulldozer.currentDirection);
        int start = Math.min(0, step * s);
        int end = Math.max(0, step * s);
        try {
            for (int a = start; a < end; ++a) {
                bulldozer.posY += s;
                LandBlock lb = map.getMap()[bulldozer.posY][bulldozer.posX];
                lb.stopOnThis = a == end - 1;
                lb.incurCost();
            }
        } catch (ArrayIndexOutOfBoundsException e) {
            throw new BulldozerInvalidMoveException("Move invalid, exceed site boundary. Program terminated.", e);
        }
    }

    @Override
    public String toString() {
        return "advance " + step;
    }
}
