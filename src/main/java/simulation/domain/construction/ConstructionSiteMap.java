package simulation.domain.construction;

import simulation.domain.site.LandBlock;

import java.util.Arrays;
import java.util.stream.Collectors;

public class ConstructionSiteMap {
    private final LandBlock[][] map;

    public ConstructionSiteMap(LandBlock[][] map) {
        this.map = map;
    }

    public ConstructionSiteMap(ConstructionSiteMap map) {
        this.map = map.getCopyMap();
    }

    public String printMap() {
        StringBuilder b = new StringBuilder();
        for (var r : map) {
            for (var e : r) {
                b.append(e).append(" ");
            }
            b.deleteCharAt(b.lastIndexOf(" ")).append("\n");
        }
        return b.toString().trim();
    }

    LandBlock[][] getMap() {
        return this.map;
    }

    public LandBlock[][] getCopyMap() {
        LandBlock[][] a = map.clone();
        return Arrays.stream(a).map(LandBlock[]::clone).collect(Collectors.toList()).toArray(a);
    }
}
