package simulation.domain.construction;

import java.util.HashMap;
import java.util.Map;

import static simulation.domain.construction.Direction.*;

public class RightCommand implements DirectionCommand {
    private static final Map<Direction, Direction> rightTurn = new HashMap<>() {{
        put(FORWARD, DOWN);
        put(DOWN, BACKWARD);
        put(BACKWARD, UP);
        put(UP, FORWARD);
    }};

    @Override
    public void execute(Bulldozer bulldozer) {
        bulldozer.currentDirection = rightTurn.get(bulldozer.currentDirection);
    }

    @Override
    public String toString() {
        return "right";
    }
}
