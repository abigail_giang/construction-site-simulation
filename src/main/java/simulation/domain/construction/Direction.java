package simulation.domain.construction;

enum Direction {
    FORWARD, BACKWARD, UP, DOWN
}