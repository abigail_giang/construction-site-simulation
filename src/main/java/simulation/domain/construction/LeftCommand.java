package simulation.domain.construction;

import java.util.HashMap;
import java.util.Map;

import static simulation.domain.construction.Direction.*;

public class LeftCommand implements DirectionCommand {
    private static final Map<Direction, Direction> leftTurn = new HashMap<>() {{
        put(FORWARD, UP);
        put(UP, BACKWARD);
        put(BACKWARD, DOWN);
        put(DOWN, FORWARD);
    }};

    @Override
    public void execute(Bulldozer bulldozer) {
        bulldozer.currentDirection = leftTurn.get(bulldozer.currentDirection);
    }

    @Override
    public String toString() {
        return "left";
    }
}


