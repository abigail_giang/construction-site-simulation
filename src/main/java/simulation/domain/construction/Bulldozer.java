package simulation.domain.construction;

import simulation.domain.site.IllegalDestructionException;

import static simulation.domain.construction.Direction.FORWARD;

public class Bulldozer {
    public Direction currentDirection = FORWARD;
    public int posX = -1;
    public int posY = 0;

    private final ConstructionSiteMap map;
    private int cmdCount = 0;

    public Bulldozer(ConstructionSiteMap map) {
        this.map = map;
    }

    public void executeCmd(DirectionCommand cmd) throws IllegalDestructionException, BulldozerInvalidMoveException {
        cmdCount++;
        cmd.execute(this);
    }

    public int getCmdCount() {
        return cmdCount;
    }

    public ConstructionSiteMap getConstructionSite() {
        return new ConstructionSiteMap(map);
    }

    ConstructionSiteMap getWorkMap() {
        return map;
    }
}


