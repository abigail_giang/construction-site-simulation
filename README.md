## How to run the program

#### Build the program:
1. Run the `gradlew task jar` to build the executable
2. The jar file is under the program `build\libs` 
3. Please run `java -jar bulldozer-simulation-1.0-SNAPSHOT.jar`

NOTE: Currently the program only shows a hard coded map for this simulation.
To change this simulation map, you can change the map input from `Application.java`
The program can be improved by reading the map layout from a different input, such as file.

#### Approach to the solution

I broke down this solution into smaller domain problems to create a solution and tests for.
There were 3 groups of related objects that I grouped together: 
 * Group 1 represents the different land types
 * Group 2 represents the bulldozer, the actions that will move the bulldozer by different commands
 * Group 3 represents those that would produce the reports for the bulldozer
 
The Land types can be constructed using Extensions, as the lands would have many common behaviours. 
There are only a few specialty in behaviours for a few type of lands, and overriding the methods in specialised class 
can customise their behaviour. 

For the bulldozer actions, I used the Command pattern to solve the problem.
These actions implemented a common interface that would allow the bulldozer to execute the command and move to the 
desired location. The implementation of each command is done in the concrete classes that presents the actions.

The Bulldozer has a dependency to the ConstructionSiteMap. I have hidden the access to the map reference so that only 
the package `construction` can modify the map content. For external packages that only consumes the map values, such as 
`cost` package, our bulldozer returns a copy of the map. This prevents unintended modification to the map.

The `cost` package implements calculation of the cost for construction. There is an interface for ReportFormatter to allow
different ways to export the format. For this implementation, we want to display to std out. Our concrete method will take 
the cost items and displays it to the screen. `ReportCostItem` classes represent the items that holds the different cost 
outputs from the calculator. This way we can always identify new items should a different cost type is requested.
 